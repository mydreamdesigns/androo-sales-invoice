<?php

namespace Androo\SalesInvoice\Model;

use Eadesigndev\Pdfgenerator\Helper\Data as DataHelper;
use Eadesigndev\Pdfgenerator\Helper\Pdf as PdfHelper;
use Eadesigndev\Pdfgenerator\Model\PdfgeneratorRepository;

class GeneratePdf
{
    /**
     * @var DataHelper
     */
    private $dataHelper;

    /**
     * @var PdfHelper
     */
    private $pdfHelper;

    /**
     * @var PdfgeneratorRepository
     */
    private $pdfGeneratorRepository;

    /**
     * Pdf constructor.
     * @param DataHelper $dataHelper
     * @param PdfHelper $pdfHelper
     */
    public function __construct(
        DataHelper $dataHelper,
        PdfHelper $pdfHelper,
        PdfgeneratorRepository $pdfgeneratorRepository
    ) {
        $this->dataHelper = $dataHelper;
        $this->pdfHelper = $pdfHelper;
        $this->pdfGeneratorRepository = $pdfgeneratorRepository;
    }

    /**
     * @param $invoice
     * @return array
     * @throws \Exception
     */
    public function makeInvoicePdf($invoice)
    {
        $template = $this->dataHelper->getTemplateStatus($invoice);
        $templateId = $template->getId();

        if (!$templateId) {
            throw new \Exception("Could find the template for the PDF for the invoice/store view.");
        }

        $templateModel = $this->pdfGeneratorRepository
            ->getById($templateId);

        if (!$templateModel) {
            throw new \Exception("Could find the template for the PDF for the invoice/store view.");
        }

        $helper = $this->pdfHelper;

        $helper->setInvoice($invoice);
        $helper->setTemplate($templateModel);

        $pdfFileParts = $helper->template2Pdf();

        return $pdfFileParts;
    }
}
