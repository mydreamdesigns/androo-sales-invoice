<?php

namespace Androo\SalesInvoice\Controller\Adminhtml\Order;

use Androo\SalesInvoice\Model\GeneratePdf;
use Androo\SalesInvoice\Controller\Adminhtml\Order;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

class PdfInvoices extends Order
{
    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var GeneratePdf
     */
    private $pdfGenerator;

    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @var DateTime
     */
    protected $dateTime;


    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        FileFactory $fileFactory,
        DateTime $dateTime,
        GeneratePdf $generatePdf
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->fileFactory = $fileFactory;
        $this->dateTime = $dateTime;
        $this->pdfGenerator = $generatePdf;

        parent::__construct($context);
    }

    /**
     * Execute action.
     *
     * If the file download occurs (valid invoices on selected orders and no exceptions etc)
     *  - the success and error messages are not rendered as the browser does not redirect/refresh.
     *  - refreshing the admin grid page will show the messages
     *
     * @return \Magento\Framework\App\ResponseInterface
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $pdfs = [];

            foreach ($collection->getItems() as $order) {
                // Your action here.
                if ($order->hasInvoices()) {
                    foreach ($order->getInvoiceCollection()->getItems() as $invoice) {
                        /**
                         * Produce PDF per invoice - or all pages from all invoices
                         */
                        try {
                            $pdfs[] = $this->pdfGenerator->makeInvoicePdf($invoice);
                        } catch (\Exception $e) {
                            $this->messageManager->addErrorMessage($e->getMessage());
                        }
                    }
                } else {
                    $this->messageManager->addErrorMessage(__('Order %1 has no invoices to print', $order->getIncrementId()));
                }
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        if (is_array($pdfs) && count($pdfs) > 0) {
            $finalPdf = new \Zend_Pdf();

            /** @var array $pdf */
            foreach ($pdfs as $pdf) {
                $zendPdf = \Zend_Pdf::parse($pdf['filestream']);

                foreach($zendPdf->pages as $pdfPage){
                    $zendPdfPage = clone $pdfPage;
                    $pdfPage =  new \Zend_Pdf_Page($zendPdfPage);
                    $finalPdf->pages[] = $pdfPage;
                }
            }

            $fileContent = ['type' => 'string', 'value' => $finalPdf->render(), 'rm' => true];

            $this->fileFactory->create(
                sprintf('BulkInvoices_%s.pdf', $this->dateTime->date('Y-m-d_H-i-s')),
                $fileContent,
                DirectoryList::VAR_DIR,
                'application/pdf'
            );
        }

        return $this->_redirect($this->_redirect->getRefererUrl());
    }
}
